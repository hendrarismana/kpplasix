<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Kerja Praktek Sistem Informasi ITS">
    <meta name="keywords" content="KP, Kerja Praktek, Sistem Informasi, ITS">
    <meta name="author" content="Hendra Rismana">

    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
    <title>Laboratorium Sistem Informasi ITS</title>

    <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#myForm").validate({
                rules: {
                    id : {
                        digits: true,
                        minlength:10,
                        maxlength:10
                    },
                }
            });
        });
    </script>

    </head>
    <body>
        <div>
            <div class="app-bar bg-blue fixed-top" data-role="appbar">
		<a class="app-bar-element" href="./">LABORATORIUM SI ITS</a>
                <div class="app-bar-element place-right">
                    <a class="fg-white" href="?menu=logout"><span class="mif-enter"></span> LOGIN</a>
                </div>
		<div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./daftarProkerHimpunanPage.php"> CONTACT US </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./daftarProkerHimpunanPage.php"> PEMINJAMAN </a></li>
                    </ul>
                </div>
                <div class="app-bar-element place-right">
                    <ul class="app-bar-menu">
                        <li><a href="./?menu=pengajuan"> HOME </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-content">
            <div class="padding20 block-shadow fg-blue" style="margin:80px 400px">
                <form id="myForm" method=post action=?menu=cek_l>
                    <h1 class="text-light">Login | LAB SI ITS</h1>
                    <hr class="thin"/>
                    <br />
                    <div class="input-control text full-size" data-role="input">
                        <label>Username</label>
                        <input type="text" id="id" name="id" maxlength="10" class="required">
                    </div>
                    <br />
                    <br />
                    <div class="input-control password full-size" data-role="input">
                        <label>Password</label>
                        <input type="password" name=password class="required">
                    </div>
                    <br />
                    <br />
                    <div class="form-actions">
                        <button type="submit" class="button primary"> Login </button>
                    </div>
                </form>
            </div>
        </div>
        <div id="footer">
            <div class="bg-blue fg-white">
                <div class="container">
                    <div class="align-center padding20 text-medium">
                        LABORATORIUM | SISTEM INFORMASI ITS
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
